﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace PhotoAlbum
{
    //thừa kế
    public class PhotoAlbum : Collection<Photo>, IDisposable
    {
        public enum DescriptorOption { FileName, Caption, DateTaken }
        private bool hasChanged = false;
        public bool HasChanged
        {
            get
            {
                if (hasChanged) return true;
                foreach (Photo p in this)//duyệt qua tất cả ảnh
                    if (p.HasChanged) return true;
                return false;
            }
            set
            {
                hasChanged = value;
                if (value == false)
                {
                    foreach (Photo p in this)
                        p.HasChanged = false;
                }
            }
        }

        public void Dispose()
        {
            ClearSettings();
            foreach (Photo p in this)
                p.Dispose();
        }

        public Photo Add(string fileName)
        {
            Photo p = new Photo(fileName);
            base.Add(p);
            return p;
        }
            
        protected override void ClearItems()
        {
            if (Count > 0)
            {
                Dispose();
                base.ClearItems();
                HasChanged = true;
            }
        }

        protected override void InsertItem(int index, Photo item)
        {
            base.InsertItem(index, item);
            HasChanged = true;
        }

        protected override void RemoveItem(int index)
        {
            Items[index].Dispose();
            base.RemoveItem(index);
            HasChanged = true;
        }

        protected override void SetItem(int index, Photo item)
        {
            base.SetItem(index, item);
            HasChanged = true;
        }

        private string title;
        private DescriptorOption descriptor;

        private void ClearSettings()
        {
            title = null;
            descriptor = DescriptorOption.Caption;
        }

        public PhotoAlbum()
        {
            ClearSettings();
        }

        public string Title
        {
            get { return title; }
            set
            {
                title = value;
                HasChanged = true;
            }
        }

        public DescriptorOption PhotoDescriptor
        {
            get { return descriptor; }
            set
            {
                descriptor = value;
                HasChanged = true;
            }
        }

        public string GetDescription(Photo photo)
        {
            switch (PhotoDescriptor)
            {
                case DescriptorOption.Caption:
                    return photo.Caption;
                case DescriptorOption.DateTaken:
                    return photo.DateTaken.ToShortDateString();
                case DescriptorOption.FileName:
                    return photo.FileName;
            }
            throw new ArgumentException(
                "Unrecognized photo descriptor option.");
        }

        public string GetDescription(int index)
        {
            return GetDescription(this[index]);
        }

        public string GetDescriptorFormat()
        {
            switch (PhotoDescriptor)
            {
                case DescriptorOption.Caption: return "c";
                case DescriptorOption.DateTaken: return "d";
                case DescriptorOption.FileName:
                default:
                    return "f";
            }
        }
    }
    
}
