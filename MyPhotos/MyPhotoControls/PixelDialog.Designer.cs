﻿namespace MyPhotoControls
{
    partial class PixelDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PixelDialog));
            this.btnClose = new System.Windows.Forms.Button();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.lbl1 = new System.Windows.Forms.Label();
            this.lblX = new System.Windows.Forms.Label();
            this.lbl2 = new System.Windows.Forms.Label();
            this.lblY = new System.Windows.Forms.Label();
            this.lblR = new System.Windows.Forms.Label();
            this.lblRed = new System.Windows.Forms.Label();
            this.lblG = new System.Windows.Forms.Label();
            this.lblGreen = new System.Windows.Forms.Label();
            this.lblB = new System.Windows.Forms.Label();
            this.lblBlue = new System.Windows.Forms.Label();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(35, 176);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 0;
            this.btnClose.Text = "&Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.lbl1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.lblX, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.lbl2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.lblY, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.lblR, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.lblRed, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.lblG, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.lblGreen, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.lblB, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.lblBlue, 1, 4);
            this.tableLayoutPanel1.GrowStyle = System.Windows.Forms.TableLayoutPanelGrowStyle.FixedSize;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(1, 1);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 5;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(142, 169);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // lbl1
            // 
            this.lbl1.AutoSize = true;
            this.lbl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl1.Location = new System.Drawing.Point(3, 0);
            this.lbl1.Name = "lbl1";
            this.lbl1.Size = new System.Drawing.Size(65, 33);
            this.lbl1.TabIndex = 0;
            this.lbl1.Text = "X:";
            this.lbl1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblX
            // 
            this.lblX.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblX.Location = new System.Drawing.Point(86, 9);
            this.lblX.Margin = new System.Windows.Forms.Padding(15, 9, 3, 0);
            this.lblX.Name = "lblX";
            this.lblX.Size = new System.Drawing.Size(40, 15);
            this.lblX.TabIndex = 0;
            this.lblX.Text = " ";
            this.lblX.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl2
            // 
            this.lbl2.AutoSize = true;
            this.lbl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl2.Location = new System.Drawing.Point(3, 33);
            this.lbl2.Name = "lbl2";
            this.lbl2.Size = new System.Drawing.Size(65, 33);
            this.lbl2.TabIndex = 1;
            this.lbl2.Text = "Y:";
            this.lbl2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblY
            // 
            this.lblY.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblY.Location = new System.Drawing.Point(86, 42);
            this.lblY.Margin = new System.Windows.Forms.Padding(15, 9, 3, 0);
            this.lblY.Name = "lblY";
            this.lblY.Size = new System.Drawing.Size(40, 15);
            this.lblY.TabIndex = 2;
            this.lblY.Text = " ";
            this.lblY.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblR
            // 
            this.lblR.AutoSize = true;
            this.lblR.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblR.Location = new System.Drawing.Point(3, 66);
            this.lblR.Name = "lblR";
            this.lblR.Size = new System.Drawing.Size(65, 33);
            this.lblR.TabIndex = 3;
            this.lblR.Text = "Red:";
            this.lblR.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblRed
            // 
            this.lblRed.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblRed.Location = new System.Drawing.Point(86, 75);
            this.lblRed.Margin = new System.Windows.Forms.Padding(15, 9, 3, 0);
            this.lblRed.Name = "lblRed";
            this.lblRed.Size = new System.Drawing.Size(40, 15);
            this.lblRed.TabIndex = 4;
            this.lblRed.Text = " ";
            this.lblRed.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblG
            // 
            this.lblG.AutoSize = true;
            this.lblG.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblG.Location = new System.Drawing.Point(3, 99);
            this.lblG.Name = "lblG";
            this.lblG.Size = new System.Drawing.Size(65, 33);
            this.lblG.TabIndex = 5;
            this.lblG.Text = "Green:";
            this.lblG.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblGreen
            // 
            this.lblGreen.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblGreen.Location = new System.Drawing.Point(86, 108);
            this.lblGreen.Margin = new System.Windows.Forms.Padding(15, 9, 3, 0);
            this.lblGreen.Name = "lblGreen";
            this.lblGreen.Size = new System.Drawing.Size(40, 15);
            this.lblGreen.TabIndex = 6;
            this.lblGreen.Text = " ";
            this.lblGreen.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblB
            // 
            this.lblB.AutoSize = true;
            this.lblB.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblB.Location = new System.Drawing.Point(3, 132);
            this.lblB.Name = "lblB";
            this.lblB.Size = new System.Drawing.Size(65, 37);
            this.lblB.TabIndex = 7;
            this.lblB.Text = "Blue:";
            this.lblB.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblBlue
            // 
            this.lblBlue.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblBlue.Location = new System.Drawing.Point(86, 141);
            this.lblBlue.Margin = new System.Windows.Forms.Padding(15, 9, 3, 0);
            this.lblBlue.Name = "lblBlue";
            this.lblBlue.Size = new System.Drawing.Size(40, 15);
            this.lblBlue.TabIndex = 8;
            this.lblBlue.Text = " ";
            this.lblBlue.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // PixelDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(144, 202);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.btnClose);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "PixelDialog";
            this.Text = "Pixel Values";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label lbl1;
        private System.Windows.Forms.Label lblX;
        private System.Windows.Forms.Label lbl2;
        private System.Windows.Forms.Label lblY;
        private System.Windows.Forms.Label lblR;
        private System.Windows.Forms.Label lblRed;
        private System.Windows.Forms.Label lblG;
        private System.Windows.Forms.Label lblGreen;
        private System.Windows.Forms.Label lblB;
        private System.Windows.Forms.Label lblBlue;
    }
}