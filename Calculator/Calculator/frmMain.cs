﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Calculator
{
    public partial class frmMain : Form
    {
        bool isTyingNumber = false; //nhập số đầu tiên
        bool ktra = false;
        bool back = false;
        enum Pheptoan { Cong, Tru, Nhan, Chia };
        Pheptoan pheptoan;
        double nho = 0.0;
        int pt = 0;
        

        public frmMain()
        {
            InitializeComponent(); //khởi tạo các thành phần, F12

        }

        private void TinhKetQua()
        {
            double ketqua = 0.0;
            switch (pheptoan)
            {
                case Pheptoan.Cong:
                    {
                        switch (pt)
                        {
                            case 1:
                                ketqua = nho + double.Parse(lblDisplay.Text);
                                lblDisplay.Text = ketqua.ToString();
                                break;
                            case 2:
                                ketqua = nho - double.Parse(lblDisplay.Text);
                                lblDisplay.Text = ketqua.ToString();
                                break;
                            case 3:
                                ketqua = nho * double.Parse(lblDisplay.Text);
                                lblDisplay.Text = ketqua.ToString();
                                break;
                            case 4:
                                ketqua = nho / double.Parse(lblDisplay.Text);
                                lblDisplay.Text = ketqua.ToString();
                                break;
                        }
                        pt = 1;
                        break;
                    }
                case Pheptoan.Tru:
                        {
                            switch (pt)
                            {
                                case 1:
                                    ketqua = nho + double.Parse(lblDisplay.Text);
                                    lblDisplay.Text = ketqua.ToString();
                                    break;
                                case 2:
                                    ketqua = nho - double.Parse(lblDisplay.Text);
                                    lblDisplay.Text = ketqua.ToString();
                                    break;
                                case 3:
                                    ketqua = nho * double.Parse(lblDisplay.Text);
                                    lblDisplay.Text = ketqua.ToString();
                                    break;
                                case 4:
                                    ketqua = nho / double.Parse(lblDisplay.Text);
                                    lblDisplay.Text = ketqua.ToString();
                                    break;
                            }
                        pt=2;
                        break;
                        }
                case Pheptoan.Nhan:
                        {
                            switch (pt)
                            {
                                case 1:
                                    ketqua = nho + double.Parse(lblDisplay.Text);
                                    lblDisplay.Text = ketqua.ToString();
                                    break;
                                case 2:
                                    ketqua = nho - double.Parse(lblDisplay.Text);
                                    lblDisplay.Text = ketqua.ToString();
                                    break;
                                case 3:
                                    ketqua = nho * double.Parse(lblDisplay.Text);
                                    lblDisplay.Text = ketqua.ToString();
                                    break;
                                case 4:
                                    ketqua = nho / double.Parse(lblDisplay.Text);
                                    lblDisplay.Text = ketqua.ToString();
                                    break;
                            }
                            pt = 3;
                            break;
                        }
                case Pheptoan.Chia:
                        {
                            switch (pt)
                            {
                                case 1:
                                    ketqua = nho + double.Parse(lblDisplay.Text);
                                    lblDisplay.Text = ketqua.ToString();
                                    break;
                                case 2:
                                    ketqua = nho - double.Parse(lblDisplay.Text);
                                    lblDisplay.Text = ketqua.ToString();
                                    break;
                                case 3:
                                    ketqua = nho * double.Parse(lblDisplay.Text);
                                    lblDisplay.Text = ketqua.ToString();
                                    break;
                                case 4:
                                    ketqua = nho / double.Parse(lblDisplay.Text);
                                    lblDisplay.Text = ketqua.ToString();
                                    break;
                            }
                            pt = 4;
                            break;
                        }
            }
        }

        private void btnBang_Click(object sender, EventArgs e)
        {
            isTyingNumber = false;
            ktra = false;
            back = false;
            TinhKetQua();
        }

        private void NhapSo(object sender, EventArgs e)
        {
            back = true;
            if (isTyingNumber)
                lblDisplay.Text = lblDisplay.Text + ((Button)sender).Text;
            else
            {
                lblDisplay.Text = ((Button)sender).Text;
                isTyingNumber = true;
            }
        }

        private void NhapPhepToan(object sender, EventArgs e)
        {
            isTyingNumber = false;
            ktra = false;
            back = false;
            switch (((Button)sender).Text)
            {
                case "+":
                    pheptoan = Pheptoan.Cong; break;
                case "-":
                    pheptoan = Pheptoan.Tru; break;
                case "*":
                    pheptoan = Pheptoan.Nhan; break;
                case "/":
                    pheptoan = Pheptoan.Chia; break;
            }
            this.btnBang_Click(sender, e);
            nho = double.Parse(lblDisplay.Text);
        }

        private void btnC_Click(object sender, EventArgs e)
        {
            lblDisplay.Text = "0.";
            isTyingNumber = false; 
            ktra = false;
            nho = 0.0;
            pt = 0;
        }
        
        private void btnBackspace_Click(object sender, EventArgs e)
        {
            if (back)
            {
                if (lblDisplay.Text.Length == 1 || lblDisplay.Text.Length == 2 && (lblDisplay.Text[0] == '-' || lblDisplay.Text == "0."))
                {
                    lblDisplay.Text = "0.";
                    isTyingNumber = false;
                }
                else
                    if (lblDisplay.Text != "")
                    {
                        lblDisplay.Text = lblDisplay.Text.Remove(lblDisplay.Text.Length - 1);
                    }
            }
        }
 
        private void btnCan_Click(object sender, EventArgs e)
        {
            lblDisplay.Text = Math.Sqrt(double.Parse(lblDisplay.Text)).ToString();
            isTyingNumber = false;
            back = false;
        }
        
        private void btnThapphan_Click(object sender, EventArgs e)
        {
            if (isTyingNumber)
            {
                if (ktra == false)
                {
                    lblDisplay.Text = lblDisplay.Text + ".";
                    ktra = true;
                }
            }
            else
            {
                lblDisplay.Text = "0.";
                isTyingNumber = true;
            }
        }

        private void btnCongTru_Click(object sender, EventArgs e)
        {
            lblDisplay.Text = (double.Parse(lblDisplay.Text) * (-1)).ToString();
        }

        private void btnPhantram_Click(object sender, EventArgs e)
        {
            lblDisplay.Text=(nho*double.Parse(lblDisplay.Text)/100).ToString();
            back = false;
        }

        private void frmMain_Load(object sender, EventArgs e)
        {

        }

        private void XuLyBanPhim_KeyPress(object sender, KeyPressEventArgs e)
        {
            switch (e.KeyChar.ToString())
            {
                case "0":
                    btn0.PerformClick(); break;
                case "1":
                    btn1.PerformClick(); break;
                case "2":
                    btn2.PerformClick(); break;
                case "3":
                    btn3.PerformClick(); break;
                case "4":
                    btn4.PerformClick(); break;
                case "5":
                    btn5.PerformClick(); break;
                case "6":
                    btn6.PerformClick(); break;
                case "7":
                    btn7.PerformClick(); break;
                case "8":
                    btn8.PerformClick(); break;
                case "9":
                    btn9.PerformClick(); break;
                case "+":
                    btnCong.PerformClick(); break;
                case "-":
                    btnTru.PerformClick(); break;
                case "*":
                    btnNhan.PerformClick(); break;
                case "/":
                    btnChia.PerformClick(); break;
                case "C":
                    btnC.PerformClick(); break;
                case "=":
                    btnBang.PerformClick(); break;
                case ".":
                    btnThapphan.PerformClick(); break;
                case "%": 
                     btnPhantram.PerformClick(); break;
            }
            
        }

        private void XuLyBanPhim_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Back)
              this.btnBackspace_Click(sender,e);
            if (e.KeyCode == Keys.Escape)
                this.Close();
        }

        
        
    }
}
