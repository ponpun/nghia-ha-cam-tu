﻿namespace Calculator
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblDisplay = new System.Windows.Forms.Label();
            this.btn7 = new System.Windows.Forms.Button();
            this.btn8 = new System.Windows.Forms.Button();
            this.btn9 = new System.Windows.Forms.Button();
            this.btn4 = new System.Windows.Forms.Button();
            this.btn5 = new System.Windows.Forms.Button();
            this.btn6 = new System.Windows.Forms.Button();
            this.btn1 = new System.Windows.Forms.Button();
            this.btn2 = new System.Windows.Forms.Button();
            this.btn3 = new System.Windows.Forms.Button();
            this.btn0 = new System.Windows.Forms.Button();
            this.btnCong = new System.Windows.Forms.Button();
            this.btnBang = new System.Windows.Forms.Button();
            this.btnTru = new System.Windows.Forms.Button();
            this.btnThapphan = new System.Windows.Forms.Button();
            this.btnNhan = new System.Windows.Forms.Button();
            this.btnChia = new System.Windows.Forms.Button();
            this.btnCongTru = new System.Windows.Forms.Button();
            this.btnCan = new System.Windows.Forms.Button();
            this.btnC = new System.Windows.Forms.Button();
            this.btnPhantram = new System.Windows.Forms.Button();
            this.btnBackspace = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblDisplay
            // 
            this.lblDisplay.BackColor = System.Drawing.Color.White;
            this.lblDisplay.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDisplay.Location = new System.Drawing.Point(12, 20);
            this.lblDisplay.Name = "lblDisplay";
            this.lblDisplay.Size = new System.Drawing.Size(174, 39);
            this.lblDisplay.TabIndex = 21;
            this.lblDisplay.Text = "0.";
            this.lblDisplay.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // btn7
            // 
            this.btn7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn7.Location = new System.Drawing.Point(15, 112);
            this.btn7.Name = "btn7";
            this.btn7.Size = new System.Drawing.Size(30, 30);
            this.btn7.TabIndex = 8;
            this.btn7.Text = "7";
            this.btn7.UseVisualStyleBackColor = true;
            this.btn7.Click += new System.EventHandler(this.NhapSo);
            this.btn7.KeyDown += new System.Windows.Forms.KeyEventHandler(this.XuLyBanPhim_KeyDown);
            this.btn7.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.XuLyBanPhim_KeyPress);
            // 
            // btn8
            // 
            this.btn8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn8.Location = new System.Drawing.Point(51, 112);
            this.btn8.Name = "btn8";
            this.btn8.Size = new System.Drawing.Size(30, 30);
            this.btn8.TabIndex = 9;
            this.btn8.Text = "8";
            this.btn8.UseVisualStyleBackColor = true;
            this.btn8.Click += new System.EventHandler(this.NhapSo);
            this.btn8.KeyDown += new System.Windows.Forms.KeyEventHandler(this.XuLyBanPhim_KeyDown);
            this.btn8.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.XuLyBanPhim_KeyPress);
            // 
            // btn9
            // 
            this.btn9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn9.Location = new System.Drawing.Point(87, 112);
            this.btn9.Name = "btn9";
            this.btn9.Size = new System.Drawing.Size(30, 30);
            this.btn9.TabIndex = 10;
            this.btn9.Text = "9";
            this.btn9.UseVisualStyleBackColor = true;
            this.btn9.Click += new System.EventHandler(this.NhapSo);
            this.btn9.KeyDown += new System.Windows.Forms.KeyEventHandler(this.XuLyBanPhim_KeyDown);
            this.btn9.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.XuLyBanPhim_KeyPress);
            // 
            // btn4
            // 
            this.btn4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn4.Location = new System.Drawing.Point(15, 148);
            this.btn4.Name = "btn4";
            this.btn4.Size = new System.Drawing.Size(30, 30);
            this.btn4.TabIndex = 5;
            this.btn4.Text = "4";
            this.btn4.UseVisualStyleBackColor = true;
            this.btn4.Click += new System.EventHandler(this.NhapSo);
            this.btn4.KeyDown += new System.Windows.Forms.KeyEventHandler(this.XuLyBanPhim_KeyDown);
            this.btn4.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.XuLyBanPhim_KeyPress);
            // 
            // btn5
            // 
            this.btn5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn5.Location = new System.Drawing.Point(51, 148);
            this.btn5.Name = "btn5";
            this.btn5.Size = new System.Drawing.Size(30, 30);
            this.btn5.TabIndex = 6;
            this.btn5.Text = "5";
            this.btn5.UseVisualStyleBackColor = true;
            this.btn5.Click += new System.EventHandler(this.NhapSo);
            this.btn5.KeyDown += new System.Windows.Forms.KeyEventHandler(this.XuLyBanPhim_KeyDown);
            this.btn5.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.XuLyBanPhim_KeyPress);
            // 
            // btn6
            // 
            this.btn6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn6.Location = new System.Drawing.Point(87, 148);
            this.btn6.Name = "btn6";
            this.btn6.Size = new System.Drawing.Size(30, 30);
            this.btn6.TabIndex = 7;
            this.btn6.Text = "6";
            this.btn6.UseVisualStyleBackColor = true;
            this.btn6.Click += new System.EventHandler(this.NhapSo);
            this.btn6.KeyDown += new System.Windows.Forms.KeyEventHandler(this.XuLyBanPhim_KeyDown);
            this.btn6.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.XuLyBanPhim_KeyPress);
            // 
            // btn1
            // 
            this.btn1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn1.Location = new System.Drawing.Point(15, 184);
            this.btn1.Name = "btn1";
            this.btn1.Size = new System.Drawing.Size(30, 30);
            this.btn1.TabIndex = 2;
            this.btn1.Text = "1";
            this.btn1.UseVisualStyleBackColor = true;
            this.btn1.Click += new System.EventHandler(this.NhapSo);
            this.btn1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.XuLyBanPhim_KeyDown);
            this.btn1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.XuLyBanPhim_KeyPress);
            // 
            // btn2
            // 
            this.btn2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn2.Location = new System.Drawing.Point(51, 184);
            this.btn2.Name = "btn2";
            this.btn2.Size = new System.Drawing.Size(30, 30);
            this.btn2.TabIndex = 3;
            this.btn2.Text = "2";
            this.btn2.UseVisualStyleBackColor = true;
            this.btn2.Click += new System.EventHandler(this.NhapSo);
            this.btn2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.XuLyBanPhim_KeyDown);
            this.btn2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.XuLyBanPhim_KeyPress);
            // 
            // btn3
            // 
            this.btn3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn3.Location = new System.Drawing.Point(87, 184);
            this.btn3.Name = "btn3";
            this.btn3.Size = new System.Drawing.Size(30, 30);
            this.btn3.TabIndex = 4;
            this.btn3.Text = "3";
            this.btn3.UseVisualStyleBackColor = true;
            this.btn3.Click += new System.EventHandler(this.NhapSo);
            this.btn3.KeyDown += new System.Windows.Forms.KeyEventHandler(this.XuLyBanPhim_KeyDown);
            this.btn3.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.XuLyBanPhim_KeyPress);
            // 
            // btn0
            // 
            this.btn0.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn0.Location = new System.Drawing.Point(15, 220);
            this.btn0.Name = "btn0";
            this.btn0.Size = new System.Drawing.Size(66, 30);
            this.btn0.TabIndex = 1;
            this.btn0.Text = "0";
            this.btn0.UseVisualStyleBackColor = true;
            this.btn0.Click += new System.EventHandler(this.NhapSo);
            this.btn0.KeyDown += new System.Windows.Forms.KeyEventHandler(this.XuLyBanPhim_KeyDown);
            this.btn0.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.XuLyBanPhim_KeyPress);
            // 
            // btnCong
            // 
            this.btnCong.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCong.Location = new System.Drawing.Point(123, 112);
            this.btnCong.Name = "btnCong";
            this.btnCong.Size = new System.Drawing.Size(30, 30);
            this.btnCong.TabIndex = 15;
            this.btnCong.Text = "+";
            this.btnCong.UseVisualStyleBackColor = true;
            this.btnCong.Click += new System.EventHandler(this.NhapPhepToan);
            this.btnCong.KeyDown += new System.Windows.Forms.KeyEventHandler(this.XuLyBanPhim_KeyDown);
            this.btnCong.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.XuLyBanPhim_KeyPress);
            // 
            // btnBang
            // 
            this.btnBang.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBang.Location = new System.Drawing.Point(159, 220);
            this.btnBang.Name = "btnBang";
            this.btnBang.Size = new System.Drawing.Size(30, 30);
            this.btnBang.TabIndex = 0;
            this.btnBang.Text = "=";
            this.btnBang.UseVisualStyleBackColor = true;
            this.btnBang.Click += new System.EventHandler(this.btnBang_Click);
            this.btnBang.KeyDown += new System.Windows.Forms.KeyEventHandler(this.XuLyBanPhim_KeyDown);
            this.btnBang.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.XuLyBanPhim_KeyPress);
            // 
            // btnTru
            // 
            this.btnTru.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTru.Location = new System.Drawing.Point(123, 148);
            this.btnTru.Name = "btnTru";
            this.btnTru.Size = new System.Drawing.Size(30, 30);
            this.btnTru.TabIndex = 14;
            this.btnTru.Text = "-";
            this.btnTru.UseVisualStyleBackColor = true;
            this.btnTru.Click += new System.EventHandler(this.NhapPhepToan);
            this.btnTru.KeyDown += new System.Windows.Forms.KeyEventHandler(this.XuLyBanPhim_KeyDown);
            this.btnTru.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.XuLyBanPhim_KeyPress);
            // 
            // btnThapphan
            // 
            this.btnThapphan.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnThapphan.Location = new System.Drawing.Point(87, 220);
            this.btnThapphan.Name = "btnThapphan";
            this.btnThapphan.Size = new System.Drawing.Size(30, 30);
            this.btnThapphan.TabIndex = 11;
            this.btnThapphan.Text = ".";
            this.btnThapphan.UseVisualStyleBackColor = true;
            this.btnThapphan.Click += new System.EventHandler(this.btnThapphan_Click);
            this.btnThapphan.KeyDown += new System.Windows.Forms.KeyEventHandler(this.XuLyBanPhim_KeyDown);
            this.btnThapphan.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.XuLyBanPhim_KeyPress);
            // 
            // btnNhan
            // 
            this.btnNhan.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNhan.Location = new System.Drawing.Point(123, 184);
            this.btnNhan.Name = "btnNhan";
            this.btnNhan.Size = new System.Drawing.Size(30, 30);
            this.btnNhan.TabIndex = 13;
            this.btnNhan.Text = "*";
            this.btnNhan.UseVisualStyleBackColor = true;
            this.btnNhan.Click += new System.EventHandler(this.NhapPhepToan);
            this.btnNhan.KeyDown += new System.Windows.Forms.KeyEventHandler(this.XuLyBanPhim_KeyDown);
            this.btnNhan.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.XuLyBanPhim_KeyPress);
            // 
            // btnChia
            // 
            this.btnChia.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnChia.Location = new System.Drawing.Point(123, 220);
            this.btnChia.Name = "btnChia";
            this.btnChia.Size = new System.Drawing.Size(30, 30);
            this.btnChia.TabIndex = 12;
            this.btnChia.Text = "/";
            this.btnChia.UseVisualStyleBackColor = true;
            this.btnChia.Click += new System.EventHandler(this.NhapPhepToan);
            this.btnChia.KeyDown += new System.Windows.Forms.KeyEventHandler(this.XuLyBanPhim_KeyDown);
            this.btnChia.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.XuLyBanPhim_KeyPress);
            // 
            // btnCongTru
            // 
            this.btnCongTru.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCongTru.Location = new System.Drawing.Point(159, 148);
            this.btnCongTru.Name = "btnCongTru";
            this.btnCongTru.Size = new System.Drawing.Size(30, 30);
            this.btnCongTru.TabIndex = 17;
            this.btnCongTru.Text = "±";
            this.btnCongTru.UseVisualStyleBackColor = true;
            this.btnCongTru.Click += new System.EventHandler(this.btnCongTru_Click);
            this.btnCongTru.KeyDown += new System.Windows.Forms.KeyEventHandler(this.XuLyBanPhim_KeyDown);
            this.btnCongTru.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.XuLyBanPhim_KeyPress);
            // 
            // btnCan
            // 
            this.btnCan.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCan.Location = new System.Drawing.Point(159, 184);
            this.btnCan.Name = "btnCan";
            this.btnCan.Size = new System.Drawing.Size(30, 30);
            this.btnCan.TabIndex = 16;
            this.btnCan.Text = "√";
            this.btnCan.UseVisualStyleBackColor = true;
            this.btnCan.Click += new System.EventHandler(this.btnCan_Click);
            this.btnCan.KeyDown += new System.Windows.Forms.KeyEventHandler(this.XuLyBanPhim_KeyDown);
            this.btnCan.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.XuLyBanPhim_KeyPress);
            // 
            // btnC
            // 
            this.btnC.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnC.Location = new System.Drawing.Point(15, 76);
            this.btnC.Name = "btnC";
            this.btnC.Size = new System.Drawing.Size(66, 30);
            this.btnC.TabIndex = 20;
            this.btnC.Text = "C";
            this.btnC.UseVisualStyleBackColor = true;
            this.btnC.Click += new System.EventHandler(this.btnC_Click);
            this.btnC.KeyDown += new System.Windows.Forms.KeyEventHandler(this.XuLyBanPhim_KeyDown);
            this.btnC.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.XuLyBanPhim_KeyPress);
            // 
            // btnPhantram
            // 
            this.btnPhantram.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPhantram.Location = new System.Drawing.Point(160, 112);
            this.btnPhantram.Name = "btnPhantram";
            this.btnPhantram.Size = new System.Drawing.Size(30, 30);
            this.btnPhantram.TabIndex = 18;
            this.btnPhantram.Text = "%";
            this.btnPhantram.UseVisualStyleBackColor = true;
            this.btnPhantram.Click += new System.EventHandler(this.btnPhantram_Click);
            this.btnPhantram.KeyDown += new System.Windows.Forms.KeyEventHandler(this.XuLyBanPhim_KeyDown);
            this.btnPhantram.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.XuLyBanPhim_KeyPress);
            // 
            // btnBackspace
            // 
            this.btnBackspace.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBackspace.Location = new System.Drawing.Point(87, 76);
            this.btnBackspace.Name = "btnBackspace";
            this.btnBackspace.Size = new System.Drawing.Size(99, 30);
            this.btnBackspace.TabIndex = 19;
            this.btnBackspace.Text = "Backspace";
            this.btnBackspace.UseVisualStyleBackColor = true;
            this.btnBackspace.Click += new System.EventHandler(this.btnBackspace_Click);
            this.btnBackspace.KeyDown += new System.Windows.Forms.KeyEventHandler(this.XuLyBanPhim_KeyDown);
            this.btnBackspace.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.XuLyBanPhim_KeyPress);
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(202, 268);
            this.Controls.Add(this.btn3);
            this.Controls.Add(this.btn2);
            this.Controls.Add(this.btnBackspace);
            this.Controls.Add(this.btnC);
            this.Controls.Add(this.btn6);
            this.Controls.Add(this.btn5);
            this.Controls.Add(this.btnThapphan);
            this.Controls.Add(this.btn0);
            this.Controls.Add(this.btn1);
            this.Controls.Add(this.btnBang);
            this.Controls.Add(this.btnChia);
            this.Controls.Add(this.btnNhan);
            this.Controls.Add(this.btnCan);
            this.Controls.Add(this.btnPhantram);
            this.Controls.Add(this.btnCongTru);
            this.Controls.Add(this.btnTru);
            this.Controls.Add(this.btnCong);
            this.Controls.Add(this.btn9);
            this.Controls.Add(this.btn4);
            this.Controls.Add(this.btn8);
            this.Controls.Add(this.btn7);
            this.Controls.Add(this.lblDisplay);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "frmMain";
            this.Text = "Calculator";
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblDisplay;
        private System.Windows.Forms.Button btn7;
        private System.Windows.Forms.Button btn8;
        private System.Windows.Forms.Button btn9;
        private System.Windows.Forms.Button btn4;
        private System.Windows.Forms.Button btn5;
        private System.Windows.Forms.Button btn6;
        private System.Windows.Forms.Button btn1;
        private System.Windows.Forms.Button btn2;
        private System.Windows.Forms.Button btn3;
        private System.Windows.Forms.Button btn0;
        private System.Windows.Forms.Button btnCong;
        private System.Windows.Forms.Button btnBang;
        private System.Windows.Forms.Button btnTru;
        private System.Windows.Forms.Button btnThapphan;
        private System.Windows.Forms.Button btnNhan;
        private System.Windows.Forms.Button btnChia;
        private System.Windows.Forms.Button btnCongTru;
        private System.Windows.Forms.Button btnCan;
        private System.Windows.Forms.Button btnC;
        private System.Windows.Forms.Button btnPhantram;
        private System.Windows.Forms.Button btnBackspace;

    }
}

